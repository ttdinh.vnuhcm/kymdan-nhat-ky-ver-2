﻿namespace ContosoUniversity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initdbver2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Nhatkyvanhanh",
                c => new
                    {
                        NhatkyvanhanhID = c.Int(nullable: false, identity: true),
                        MotaNhatky = c.String(),
                        Thongtinsuco = c.String(),
                        Luuluongvao = c.Single(nullable: false),
                        Luuluongra = c.Single(nullable: false),
                        Chisotieuthudien = c.Single(nullable: false),
                        Khoiluongtieuthudien = c.Single(nullable: false),
                        polymervao = c.Single(nullable: false),
                        polymerra = c.Single(nullable: false),
                        phabotvao = c.Single(nullable: false),
                        phabotra = c.Single(nullable: false),
                        NguoiTruc = c.String(),
                    })
                .PrimaryKey(t => t.NhatkyvanhanhID);
            
            CreateTable(
                "dbo.OperationLog",
                c => new
                    {
                        OperationLogID = c.Int(nullable: false, identity: true),
                        OperationCode = c.String(),
                        Action = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ReferenceTable = c.String(),
                    })
                .PrimaryKey(t => t.OperationLogID);
            
            CreateTable(
                "dbo.ThongtinMe",
                c => new
                    {
                        ThongTinMeID = c.Int(nullable: false, identity: true),
                        ThongTinMeCode = c.String(),
                        CreateBy = c.String(),
                        CreateDate = c.DateTime(),
                        UpdateBy = c.String(),
                        UpdateDate = c.DateTime(),
                        Begom = c.String(),
                        CycloTron = c.String(),
                        Belang = c.String(),
                        BomHoaChat = c.String(),
                        BeDieuHoa = c.String(),
                        BeSBR1 = c.String(),
                        BeSBR2 = c.String(),
                        MayEpBun = c.String(),
                        identityIdMe = c.String(),
                        idMe = c.Int(nullable: false),
                        dayTimeFrom = c.DateTime(),
                        dayTimeTo = c.DateTime(),
                        userUpdateMe = c.String(),
                        timeUpdateMe = c.DateTime(),
                        begomPh = c.String(),
                        begomHoahocCod = c.String(),
                        begomHoahocTss = c.String(),
                        begomHoahocNt = c.String(),
                        begomHoahocP = c.String(),
                        begomTimeBom = c.DateTime(),
                        cyclotronTimeXaday = c.DateTime(),
                        belangTimeXaday = c.DateTime(),
                        bomhoachatNongdo = c.String(),
                        bomhoachatTimeBom = c.DateTime(),
                        bedieuhoaDo = c.String(),
                        bedieuhoaTimeBom = c.DateTime(),
                        bedieuhoaTimeThoikhi = c.DateTime(),
                        besbr1Ph = c.String(),
                        besbr1Do = c.String(),
                        besbr1HoahocCod = c.String(),
                        besbr1HoahocTss = c.String(),
                        besbr1HoahocNt = c.String(),
                        besbr1HoahocP = c.String(),
                        besbr1Vl = c.String(),
                        besbr1TimeLamday = c.DateTime(),
                        besbr1TimeXuly = c.DateTime(),
                        besbr1TimeLang = c.DateTime(),
                        besbr1TimeXa = c.DateTime(),
                        besbr1TimeCho = c.DateTime(),
                        besbr1TimeBombun = c.DateTime(),
                        besbr2Ph = c.String(),
                        besbr2Do = c.String(),
                        besbr2HoahocCod = c.String(),
                        besbr2HoahocTss = c.String(),
                        besbr2HoahocNt = c.String(),
                        besbr2HoahocP = c.String(),
                        besbr2Vl = c.String(),
                        besbr2TimeLamday = c.DateTime(),
                        besbr2TimeXuly = c.DateTime(),
                        besbr2TimeLang = c.DateTime(),
                        besbr2TimeXa = c.DateTime(),
                        besbr2TimeCho = c.DateTime(),
                        besbr2TimeBombun = c.DateTime(),
                        mayepbunTimeHoatdong = c.DateTime(),
                        mayepbunKhoiluongbunkho = c.String(),
                    })
                .PrimaryKey(t => t.ThongTinMeID);
            
            CreateTable(
                "dbo.ThongtinSuCo",
                c => new
                    {
                        ThongtinSuCoID = c.Int(nullable: false, identity: true),
                        ThongTinSuCoCode = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        CreateBy = c.String(),
                        UpdateDate = c.DateTime(nullable: false),
                        UpdateBy = c.String(),
                        LoaiSuCo = c.String(),
                        MoTaNganSuCo = c.String(),
                        NguoiMota = c.String(),
                        Cachkhacphuc = c.String(),
                        NguoiDeNghiKhacPhuc = c.String(),
                        Ketqua = c.String(),
                        NhatKyID = c.Int(),
                    })
                .PrimaryKey(t => t.ThongtinSuCoID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ThongtinSuCo");
            DropTable("dbo.ThongtinMe");
            DropTable("dbo.OperationLog");
            DropTable("dbo.Nhatkyvanhanh");
        }
    }
}
