﻿using ContosoUniversity.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ContosoUniversity.DAL
{
    public class SchoolContext : DbContext
    {

        public DbSet<Nhatkyvanhanh> Nhatkyvanhanhs { get; set; }

        public DbSet<OperationLog> OperationLogs { get; set; }
        public DbSet<ThongtinMe> ThongtinMes { get; set; }
        public DbSet<ThongtinSuCo> ThongtinSuCos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        }
    }
}