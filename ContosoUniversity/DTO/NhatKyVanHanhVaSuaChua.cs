﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContosoUniversity.DTO
{
    public class NhatKyVanHanhVaSuaChua
    {
        public int IDNhatKy { get; set; }
        public string MaNhatKy { get; set; }
        [Display(Name = "Ngày tạo")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime NgayTao { get; set; }
        [Display(Name = "Chú ý ca sau theo dõi")]
        public string ChuYCaSauTheoDoi { get; set; }
        [Display(Name = "Người tạo")]
        public string NguoiTao { get; set; }
        [Display(Name = "Danh sách mẻ")]
        public List<string> DanhSachMe { get; set; }
        [Display(Name = "Nhập ghi chú ca sau")]
        public string NhapGhiChuChoCaSau { get; set; }
        [Display(Name = "Ca làm việc")]
        public string CaLamViec { get; set; }

        //public List<ChiTietNhatKy> DanhSachNhatKy { get; set; }
        [Display(Name = "Lưu lượng vào(l)")]
        public decimal? LuuLuongVao { get; set; }
        [Display(Name = "Lưu lượng ra(l)")]
        public decimal? LuuLuongRa { get; set; }
        [Display(Name = "Chỉ số tiêu thụ điện(kw)")]
        public decimal? ChiSoTieuThuDien { get; set; }
        [Display(Name = "KL tiêu thụ điện(kl)")]
        public decimal? KhoiLuongTieuThuDien { get; set; }
        [Display(Name = "Polymer vào(kg)")]
        public decimal? PolymerVao { get; set; }
        [Display(Name = "Polymer ra(kg)")]
        public decimal? PolymerRa { get; set; }
        [Display(Name = "Polymer tồn(kg)")]
        public decimal? PolymerTon { get; set; }
        [Display(Name = "Phá bọt vào(kg)")]
        public decimal? PhaBotVao { get; set; }
        [Display(Name = "Phá bọt ra(kg)")]
        public decimal? PhaBotRa { get; set; }
        [Display(Name = "Phá bọt tồn(kg)")]
        public decimal? PhaBotTon { get; set; }
        [Display(Name = "NaOH vào(kg)")]
        public decimal? NaOHVao { get; set; }
        [Display(Name = "NaOH ra(kg)")]
        public decimal? NaOHRa { get; set; }
        [Display(Name = "NaOH tồn(kg)")]
        public decimal? NaOHTon { get; set; }
        [Display(Name = "NPK vào(kg)")]
        public decimal? NPKVao { get; set; }
        [Display(Name = "NPK ra(kg)")]
        public decimal? NPKRa { get; set; }
        [Display(Name = "NPK tồn(kg)")]
        public decimal? NPKTon { get; set; }
        [Display(Name = "Ure vào(kg)")]
        public decimal? UreVao { get; set; }
        [Display(Name = "Ure ra(kg)")]
        public decimal? UreRa { get; set; }


        [Display(Name = "Ure tồn(kg)")]
        public decimal? UreTon { get; set; }
        [Display(Name = "Mật rỉ vào(kg)")]
        public decimal? MatRiVao { get; set; }
        [Display(Name = "Mật rỉ ra(kg)")]
        public decimal? MatRiRa { get; set; }
        [Display(Name = "Mật rỉ tồn(kg)")]
        public decimal? MatRiTon { get; set; }
        public bool IsError { get; set; }

        //Thông tin sự cố
        public DateTime NgayThemSuCo { get; set; }
        [Display(Name = "Thời gian vận hành")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ThoiGianVanHanh { get; set; }
        [Display(Name = "Chỉ số hóa học")]
        public string ChiSoHoaHoc { get; set; }
        [Display(Name = "Hư hỏng thiết bị")]
        public string HuHongThietBi { get; set; }
        [Display(Name = "Mô tả sự cố")]
        public string MoTaSuCo { get; set; }
        [Display(Name = "Cách khắc phục")]
        public string CachKhacPhuc { get; set; }
        [Display(Name = "Người đề xuất")]
        public string NguoiDeXuat { get; set; }
        [Display(Name = "Kết quả")]
        public string KetQua { get; set; }
        public List<ThongTinSuCo> DanhSachSuCo { get; set; }

    }
    //public class ChiTietNhatKy
    //{
    //    public int? LuuLuongVao { get; set; }
    //    public int? LuuLuongRa { get; set; }
    //    public int? ChiSoTieuThuDien { get; set; }
    //    public int? KhoiLuongTieuThuDien { get; set; }

    //    public int? PolymerVao { get; set; }
    //    public int? PolymerRa { get; set; }
    //    public int? PolymerTon { get; set; }

    //    public int? PhaBotVao { get; set; }
    //    public int? PhaBotRa { get; set; }
    //    public int? PhaBotTon { get; set; }

    //    public int? NaOHVao { get; set; }
    //    public int? NaOHRa { get; set; }
    //    public int? NaOHTon { get; set; }

    //    public int? NPKVao { get; set; }
    //    public int? NPKRa { get; set; }
    //    public int? NPKTon { get; set; }

    //    public int? UreVao { get; set; }
    //    public int? UreRa { get; set; }
    //    public int? UreTon { get; set; }

    //    public int? MatRiVao { get; set; }
    //    public int? MatRiRa { get; set; }
    //    public int? MatRiTon { get; set; }
    //    public bool IsError { get; set; }
    //    public List<ThongTinSuCo> DanhSachSuCo { get; set; }

    //}
    public class ThongTinSuCo
    {
        public DateTime NgayTaoSuCo { get; set; }
        public DateTime ThoiGianVanHanh { get; set; }
        public string ChiSoHoaHoc { get; set; }
        public string HuHongThietBi { get; set; }
        public string MoTaSuCo { get; set; }
        public string CachKhacPhuc { get; set; }
        public string NguoiDeXuat { get; set; }
        public string KetQua { get; set; }
        public int SuCoID { get; set; }
    }
}