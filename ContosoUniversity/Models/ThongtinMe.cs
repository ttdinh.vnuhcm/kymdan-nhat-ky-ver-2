﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Models
{
    public class ThongtinMe
    {
        public int? ThongTinMeID { get; set; }

        public string ThongTinMeCode { get; set; }
        public string CreateBy { get; set; }

        [Display(Name = "Thời gian tạo mẻ")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? CreateDate { get; set; }
        public string UpdateBy { get; set; }

        [Display(Name = "Thời gian cập nhật mẻ")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? UpdateDate { get; set; }
        public string Begom { get; set; }
        public string CycloTron { get; set; }
        public string Belang { get; set; }
        public string BomHoaChat { get; set; }
        public string BeDieuHoa { get; set; }
        public string BeSBR1 { get; set; }
        public string BeSBR2 { get; set; }
        public string MayEpBun { get; set; }

        /// <summary>
        /// INFO ME
        /// </summary>
        public string identityIdMe { get; set; }

        [Display(Name = "ID mẻ")]
        public int idMe { get; set; }

        [Display(Name = "Thời gian bắt đầu mẻ")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? dayTimeFrom { get; set; }

        [Display(Name = "Thời gian kết thúc mẻ")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? dayTimeTo { get; set; }

        [Display(Name = "Người cập nhật")]
        public string userUpdateMe { get; set; }

        [Display(Name = "Thời gian cập nhật dữ liệu")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm}")]
        public DateTime? timeUpdateMe { get; set; }

        /// <summary>
        /// Thông tin chính của mẻ
        /// </summary>
        /// 
        [Display(Name = "pH")]
        public string begomPh { get; set; }

        [Display(Name = "COD")]
        public string begomHoahocCod { get; set; }
        [Display(Name = "TSS")]
        public string begomHoahocTss { get; set; }
        [Display(Name = "Nt")]
        public string begomHoahocNt { get; set; }
        [Display(Name = "P")]
        public string begomHoahocP { get; set; }

        [Display(Name = "Thời gian bơm")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? begomTimeBom { get; set; }

        /// <summary>
        /// Cyclo trộn của mẻ
        /// </summary>

        [Display(Name = "Thời gian xả đáy")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? cyclotronTimeXaday { get; set; }

        /// <summary>
        /// Bể lắng của mẻ
        /// </summary>

        [Display(Name = "Thời gian xả đáy")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? belangTimeXaday { get; set; }

        /// <summary>
        /// Bơm hoá chất của mẻ
        /// </summary>
        /// 
[Display(Name = "Nồng độ hoá chất")]
        public string bomhoachatNongdo { get; set; }

        [Display(Name = "Thời gian bơm")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? bomhoachatTimeBom { get; set; }

        /// <summary>
        /// Bể điều hoà của mẻ
        /// </summary>
        /// 
        [Display(Name = "DO")]        
        public string bedieuhoaDo { get; set; }

        [Display(Name = "Thời gian bơm")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? bedieuhoaTimeBom { get; set; }

        [Display(Name = "Thời gian thổi khí")]
        [DataType(DataType.Date)]        
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? bedieuhoaTimeThoikhi { get; set; }

        /// <summary>
        /// Bể SBR của mẻ
        /// </summary>
        /// 
        [Display(Name = "pH")]
        public string besbr1Ph { get; set; }

        [Display(Name = "DO")]
        public string besbr1Do { get; set; }

        [Display(Name = "COD")]
        public string besbr1HoahocCod { get; set; }

        [Display(Name = "TSS")]
        public string besbr1HoahocTss { get; set; }

        [Display(Name = "Nt")]
        public string besbr1HoahocNt { get; set; }

        [Display(Name = "P")]
        public string besbr1HoahocP { get; set; }

        [Display(Name = "VL")]
        public string besbr1Vl { get; set; }

        [Display(Name = "Thời gian làm đầy")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr1TimeLamday { get; set; }

        [Display(Name = "Thời gian xử lí")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr1TimeXuly { get; set; }

        [Display(Name = "Thời gian lắng")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr1TimeLang { get; set; }

        [Display(Name = "Thời gian xả")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr1TimeXa { get; set; }

        [Display(Name = "Thời gian chờ")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr1TimeCho { get; set; }

        [Display(Name = "Thời gian bơm bùn")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr1TimeBombun { get; set; }

        /// <summary>
        /// Bể SBR 2 của mẻ
        /// </summary>
        /// 
        [Display(Name = "pH")]
        public string besbr2Ph { get; set; }

        [Display(Name = "DO")]
        public string besbr2Do { get; set; }

        [Display(Name = "COD")]
        public string besbr2HoahocCod { get; set; }

        [Display(Name = "TSS")]
        public string besbr2HoahocTss { get; set; }

        [Display(Name = "Nt")]
        public string besbr2HoahocNt { get; set; }

        [Display(Name = "P")]
        public string besbr2HoahocP { get; set; }

        [Display(Name = "VL")]
        public string besbr2Vl { get; set; }

        [Display(Name = "Thời gian làm đầy")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr2TimeLamday { get; set; }

        [Display(Name = "Thời gian xử lí")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr2TimeXuly { get; set; }

        [Display(Name = "Thời gian lắng")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr2TimeLang { get; set; }

        [Display(Name = "Thời gian xả")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr2TimeXa { get; set; }

        [Display(Name = "Thời gian chờ")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr2TimeCho { get; set; }

        [Display(Name = "Thời gian bơm bùn")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? besbr2TimeBombun { get; set; }


        /// <summary>
        /// Máy ép bùn của mẻ
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? mayepbunTimeHoatdong { get; set; }
        public string mayepbunKhoiluongbunkho { get; set; }
    }
}