﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ContosoUniversity.Models
{
    public class ThongtinSuCo
    {
        public int ThongtinSuCoID { get; set; }
        public string ThongTinSuCoCode { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        public string LoaiSuCo { get; set; }
        public string MoTaNganSuCo { get; set; }
        public string NguoiMota { get; set; }
        public string Cachkhacphuc { get; set; }
        public string NguoiDeNghiKhacPhuc { get; set; }
        public string Ketqua { get; set; }
        public int? NhatKyID { get; set; }

    }
}