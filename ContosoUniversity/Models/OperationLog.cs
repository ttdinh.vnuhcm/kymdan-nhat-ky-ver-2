﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;



namespace ContosoUniversity.Models
{
    public class OperationLog
    {
        public int OperationLogID { get; set; }
        public string OperationCode { get; set; }
        public string Action { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ReferenceTable { get; set; }

    }
}