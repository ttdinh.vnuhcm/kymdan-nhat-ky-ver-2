﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContosoUniversity.Common;
using ContosoUniversity.DAL;
using ContosoUniversity.DTO;
using ContosoUniversity.Models;
using Enum = System.Enum;

namespace ContosoUniversity.Controllers
{
    public class NhatkyvanhanhsController : Controller
    {
        private SchoolContext db = new SchoolContext();

        // GET: Nhatkyvanhanhs
        public ActionResult Index()
        {
            return View(db.Nhatkyvanhanhs.ToList());
        }

        // GET: Nhatkyvanhanhs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nhatkyvanhanh nhatkyvanhanh = db.Nhatkyvanhanhs.Find(id);
            if (nhatkyvanhanh == null)
            {
                return HttpNotFound();
            }
            return View(nhatkyvanhanh);
        }

        // GET: Nhatkyvanhanhs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Nhatkyvanhanhs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NhatkyvanhanhID,MotaNhatky,Thongtinsuco,Luuluongvao,Luuluongra,Chisotieuthudien,Khoiluongtieuthudien,polymervao,polymerra,phabotvao,phabotra,NguoiTruc")] Nhatkyvanhanh nhatkyvanhanh)
        {
            if (ModelState.IsValid)
            {
                db.Nhatkyvanhanhs.Add(nhatkyvanhanh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(nhatkyvanhanh);
        }

        // GET: Nhatkyvanhanhs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nhatkyvanhanh nhatkyvanhanh = db.Nhatkyvanhanhs.Find(id);
            if (nhatkyvanhanh == null)
            {
                return HttpNotFound();
            }
            return View(nhatkyvanhanh);
        }

        // POST: Nhatkyvanhanhs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "NhatkyvanhanhID,MotaNhatky,Thongtinsuco,Luuluongvao,Luuluongra,Chisotieuthudien,Khoiluongtieuthudien,polymervao,polymerra,phabotvao,phabotra,NguoiTruc")] Nhatkyvanhanh nhatkyvanhanh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nhatkyvanhanh).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nhatkyvanhanh);
        }

        // GET: Nhatkyvanhanhs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nhatkyvanhanh nhatkyvanhanh = db.Nhatkyvanhanhs.Find(id);
            if (nhatkyvanhanh == null)
            {
                return HttpNotFound();
            }
            return View(nhatkyvanhanh);
        }

        // POST: Nhatkyvanhanhs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Nhatkyvanhanh nhatkyvanhanh = db.Nhatkyvanhanhs.Find(id);
            db.Nhatkyvanhanhs.Remove(nhatkyvanhanh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #region Nhật ký vận hành và sữa chữa
        public ActionResult NhatKyVanHanhVaSuCo()
        {
            List<NhatKyVanHanhVaSuaChua> lstNhatKyVanHanh = new List<NhatKyVanHanhVaSuaChua>();
            var getDSNhatKy = db.Nhatkyvanhanhs.ToList();
            var getAllSuCo = db.ThongtinSuCos.ToList();
            var groupJoin = getDSNhatKy.GroupJoin(getAllSuCo, nk=>nk.NhatkyvanhanhID, sc => sc.NhatKyID,(nk,sc) => new { nk, sc});
            foreach (var item in groupJoin)
            {
                NhatKyVanHanhVaSuaChua nhatkyvanhanh = new NhatKyVanHanhVaSuaChua
                {
                    IDNhatKy = item.nk.NhatkyvanhanhID,
                    LuuLuongVao = Convert.ToDecimal(item.nk.Luuluongvao),
                    LuuLuongRa = Convert.ToDecimal(item.nk.Luuluongra),
                    ChiSoTieuThuDien = Convert.ToDecimal(item.nk.Chisotieuthudien),
                    KhoiLuongTieuThuDien = Convert.ToDecimal(item.nk.Khoiluongtieuthudien),
                    PolymerVao = Convert.ToDecimal(item.nk.polymervao),
                    PolymerRa = Convert.ToDecimal(item.nk.polymerra),
                    PolymerTon = Convert.ToDecimal(item.nk.polymervao) - Convert.ToDecimal(item.nk.polymerra),
                    PhaBotVao = Convert.ToDecimal(item.nk.phabotvao),
                    PhaBotRa = Convert.ToDecimal(item.nk.polymerra),
                    PhaBotTon = Convert.ToDecimal(item.nk.phabotvao) - Convert.ToDecimal(item.nk.phabotra),
                    ChuYCaSauTheoDoi = item.nk.MotaNhatky

                };
                lstNhatKyVanHanh.Add(nhatkyvanhanh);
                nhatkyvanhanh.DanhSachSuCo = new List<ThongTinSuCo>();
                foreach (var suco in item.sc)
                {
                    ThongTinSuCo thongTinSuCo = new ThongTinSuCo()
                    {
                        SuCoID = suco.ThongtinSuCoID,
                        NgayTaoSuCo = suco.CreateDate,
                        MoTaSuCo = suco.MoTaNganSuCo,
                        KetQua = suco.Ketqua,
                        CachKhacPhuc = suco.Cachkhacphuc
                    };
                    nhatkyvanhanh.DanhSachSuCo.Add(thongTinSuCo);
                }
            }
            ViewBag.lstNhatKy = lstNhatKyVanHanh;

            //foreach (var nhatKyModel in getDSNhatKy)
            //{
            //    NhatKyVanHanhVaSuaChua nhatKy = new NhatKyVanHanhVaSuaChua
            //    {
            //        IDNhatKy = nhatKyModel.NhatkyvanhanhID,
            //        LuuLuongVao = Convert.ToDecimal(nhatKyModel.Luuluongvao),
            //        LuuLuongRa = Convert.ToDecimal(nhatKyModel.Luuluongra),
            //        ChiSoTieuThuDien = Convert.ToDecimal(nhatKyModel.Chisotieuthudien),
            //        KhoiLuongTieuThuDien = Convert.ToDecimal(nhatKyModel.Khoiluongtieuthudien),
            //        PolymerVao = Convert.ToDecimal(nhatKyModel.polymervao),
            //        PolymerRa = Convert.ToDecimal (nhatKyModel.polymerra),
            //        PolymerTon = Convert.ToDecimal(nhatKyModel.polymervao) - Convert.ToDecimal(nhatKyModel.polymerra),
            //        PhaBotVao = Convert.ToDecimal(nhatKyModel.phabotvao),
            //        PhaBotRa = Convert.ToDecimal(nhatKyModel.polymerra),
            //        PhaBotTon = Convert.ToDecimal(nhatKyModel.phabotvao) - Convert.ToDecimal(nhatKyModel.phabotra),
            //        ChuYCaSauTheoDoi = nhatKyModel.MotaNhatky

            //    };
            //    lstNhatKyVanHanh.Add(nhatKy);
            //}
            //ViewBag.lstNhatKy = lstNhatKyVanHanh;
            var enumCalamviec = from Calamviec ca in Enum.GetValues(typeof(Calamviec))
                                select new
                                {
                                    ID = (int)ca,
                                    Name = ca.ToString()
                                };
            ViewBag.EnumCaLamViec = enumCalamviec;//new SelectList(enumCalamviec, "ID", "Name");

            return View("NhatKyVanHanhVaSuaChua");
        }
        public ActionResult AddNewNhatKy()
        {
            return PartialView("~/Views/Nhatkyvanhanhs/AddNhatKyVanHanh.cshtml");

        }
        
        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
