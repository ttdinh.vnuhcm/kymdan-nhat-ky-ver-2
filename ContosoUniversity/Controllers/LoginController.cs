﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContosoUniversity.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
       
        public ActionResult LoginIndex()
        {
            return View();

        }
        public ActionResult LogOut()
        {
            return RedirectToAction("LoginIndex", "Login");

        }
    }
}